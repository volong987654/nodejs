import db from '../models/index';
import bcrypt from 'bcryptjs';
import { raw } from 'body-parser';
import { reject } from 'bcrypt/promises';
const salt = bcrypt.genSaltSync(10);


let hasdUserPassword = (password) => {
    return new Promise(async (resolve, reject) => {
        try {
            let hasdUserPassword = bcrypt.hashSync(password, salt);
            resolve(hasdUserPassword);
        } catch (e) {
            reject(e);
        }
    })
}
let handleUserLogin = (email, password) => {
    return new Promise(async (resolve, reject) => {
        try {
            let userData = {};
            let isExit = await checkUserEmail(email);
            if (isExit) {
                // user already exist
                //comprae password
                let user = await db.User.findOne({
                    // attributes: ['email', 'roleId'],
                    where: { email: email },
                    raw: true

                });
                if (user) {
                   let check = await bcrypt.compareSync(password,user.password);
                   if(check){
                       userData.errCode = 0,
                       userData.errMessage = 'Ok',
                       console.log(user)
                       delete user.password,
                       userData.user = user;
                   } else{
                    userData.errCode = 3,
                    userData.errMessage = 'Wrong password'
                   }
                } else {
                    userData.errCode = 2;
                    userData.errMessage = 'Email not found'
                }
            } else {
                //return errCode
                userData.errorCode = 1;
                userData.errMessage = 'Email ko tồn tại'

            }
            resolve(userData)
        } catch (e) {
            reject(e)

        }
    })
}
let checkUserEmail = (userEmail) => {
    return new Promise(async (resolve, reject) => {
        try {
            let user = await db.User.findOne({
                where: { email: userEmail }
            })
            if (user) {
                resolve(true)
            } else {
                resolve(false)
            }
        } catch (e) {
            reject(e)

        }
    })
}
let getAllUsers = (userId) => {
    return new Promise(async (resolve, reject) => {
        try {
            let users = '';
           if(userId === 'ALL'){
               users = db.User.findAll({
                   attributes:{
                       exclude:['password']
                   }
               })
           } 
           if(userId && userId !== 'ALL'){
               users = await db.User.findOne({
                   where:{ id: userId},
                   attributes:{
                    exclude:['password']
                }
               })
           }
           resolve(users)
        } catch (e) {
            reject(e)

        }
    })
}
let createNewUser = (data) =>{
    return new Promise(async (resolve, reject) => {
        try {
            // check email tồn tại chưa
            let check = await checkUserEmail(data.email);
            if( check === true){
                resolve(
                    {
                     errCode: 1,
                     message:'Email đã tồn tại'
                    })
            } else {
                let hasdPasswordFormBcrypt = await hasdUserPassword(data.password);
                await db.User.create({
                    email: data.email,
                    password: hasdPasswordFormBcrypt,
                    firstName: data.firstName,
                    lastName: data.lastName,
                    address: data.address,
                    gender: data.gender,
                    roleId: data.roleId,
                    phonenumber: data.phonenumber,
                    positionId:data.positionId
                })
                resolve(
                   {
                    errCode: 0,
                    message:'Ok'
                   }
                ) 
            } 
        } catch (e) {
            reject(e)

        }
    })
}
let deleteUser = (userId) => {
    return new Promise(async (resolve, reject) => {
        let user = await db.User.findOne({
            where:{ id:userId }
        })
        if(!user){
            resolve({
                errCode:2,
                errMessage:'User ko tồn tại'
            })
        }
        // if(user){
        //     await user.destroy();
        // }
        await db.User.destroy({
            where:{ id:userId }
        })
        resolve({
            errCode:0,
            errMessage:'User đã bị xóa'
        })
    })
}
let updateUserData = (data) => {
    return new Promise(async (resolve, reject) => {
        try {
            if(!data.id){
                resolve(
                    {
                     errCode:2,
                     message:"Messing lõi"
                    }
                    )
               }
            let user = await db.User.findOne({
                where :{ id:data.id},
                raw:false
           })
           if(user){
            user.firstName = data.firstName;
            user.lastName = data.lastName;
            user.address = data.address;
               await user.save();
               resolve({
                   errCode:0,
                   message:"Update thành công"
               })
           } else{
               resolve(
               {
                errCode:1,
                message:"Update ko thành công"
               }
               )
           }
            }
         catch (e) {
            reject(e);
        }
    })
}

let getAllCodeService = (typeInput) => {
    return new Promise( async (resolve, reject) =>{
        try {
            if(!typeInput){
                resolve({
                    errCode : 1,
                    errMessage : "Missing required paraters"
                })
            } else {
                let res = {};// biến hứng
                let allcode = await db.allcodes.findAll({
                    where:{type:typeInput}
                });
                res.errCode = 0;
                res.data = allcode;
                resolve(res);
            } 
        } catch (e) {
            reject(e);
            
        }
    })
}
module.exports = {
    handleUserLogin: handleUserLogin,
    getAllUsers:getAllUsers,
    createNewUser: createNewUser,
    deleteUser:deleteUser,
    updateUserData:updateUserData,
    getAllCodeService:getAllCodeService
}