import db from '../models/index'
import CrudService from '../services/CrudService'

let getHomePage = async (req, res) => {
    try {
        let data = await db.User.findAll();
        return res.render('homepage.ejs', {
            data: JSON.stringify(data)
        });
    } catch (e) {
        console.log(e)
    }
}
let getAboutPage = (req, res) => {
    return res.render('test/about.ejs');
}

let getCRUD = (req, res) => {
    return res.render('crud.ejs');
}
let postCRUD = async (req, res) => {
    let message = await CrudService.createNewUser(req.body);
    console.log(message)
    return res.send('post server');
}
let displayGetCRUD = async (req, res) => {
    let data = await CrudService.getAllUser();
    return res.render('displaygetCrud.ejs', {
        data: data
    });
}
let getEditCRUD = async (req, res) => {
    let userId = req.query.id;
    if(userId){
        let userData = await CrudService.getUserInfoById(userId);
       if(userData )
        return res.render('edit-crud.ejs',{
            user:userData
        });
    } else {
        return res.send('User notfound');
    } 
}
let pudCRUD = async (req, res) => {
   let data = req.body;
   let allUsers = await CrudService.updateUserData(data);
   return res.render('displaygetCrud.ejs', {
    data: allUsers
});
}
let deleteCRUD = async (req, res) => {
   let id = req.query.id;
   if(id){
    await CrudService.deleteUserById(id);
    return res.render('displaygetCrud.ejs');
   } else {
    return res.send('ko có Id');
   }
  
//    return res.render('displaygetCrud.ejs');
}

module.exports = {
    getHomePage: getHomePage,
    getAboutPage: getAboutPage,
    getCRUD: getCRUD,
    postCRUD: postCRUD,
    displayGetCRUD: displayGetCRUD,
    getEditCRUD: getEditCRUD,
    pudCRUD:pudCRUD,
    deleteCRUD:deleteCRUD,
}