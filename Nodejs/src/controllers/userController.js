import userService from '../services/userService'
let handleLogin =  async (req,res) =>{
    let email = req.body.email;
    let password = req.body.password;
    if(!email || !password ){
        return res.status(500).json({
            errCode:1,
            message:'Missing inputs parametor!',
        })
    }
    let userData = await userService.handleUserLogin(email,password);
     return res.status(200).json({
         errCode:userData.errCode,
         message:userData.errMessage,
         user:userData.user ? userData.user : {}
        // userData
     })
}
let handleGetAllUsers =  async (req,res) =>{
    let id = req.query.id; // All, id

    if(!id){
        return res.status(200).json({
            errCode:1,
            errMessage:'Messing required parameters',
            users:[]
        })
    }
   let users = await userService.getAllUsers(id);
   return res.status(200).json({
       errCode:0,
       errMessage:'ok',
       users
   })
}
let createhandleCreateNewUser = async (req,res)=>{
    let message = await userService.createNewUser(req.body);
    console.log(message)
     return res.status(200).json(message);

}
let handleEditUser = async (req,res)=>
    {
        let data = req.body;
        let message = await userService.updateUserData(data);
        return res.status(200).json(message);
     }
let handleDeleteUser = async (req,res)=>{
    if(!req.body.id) {
        return res.status(200).json({
            errCode:1,
            errMessage:"Truyền Id vào "
        });
    }
    let message = await userService.deleteUser(req.body.id);
    console.log(message)
     return res.status(200).json(message);

}

let getAllcode = async (req, res) =>{
    try {
        let data = await userService.getAllCodeService(req.query.type);// truy vấn dk type
        console.log(data)
        return res.status(200).json(data);
        
    } catch (e) {
        console.log('get all code',e)
        return res.status(200).json({
            errCode: -1,
            errMessage: 'Error from server'
        })
        
    }
}
module.exports ={
    handleLogin : handleLogin,
    handleGetAllUsers:handleGetAllUsers,
    createhandleCreateNewUser:createhandleCreateNewUser,
    handleEditUser:handleEditUser,
    handleDeleteUser:handleDeleteUser,
    getAllcode:getAllcode
}